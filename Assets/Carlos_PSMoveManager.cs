﻿using UnityEngine;
using System.Collections;

public class Carlos_PSMoveManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
       // PSMoveAPI.psmove_fusion_new(0, 0, 500);
	}
	
	// Update is called once per frame
	void Update () {
        PSMoveDataContext[] moveCtrl = new PSMoveDataContext[2];
        moveCtrl[0] = PSMoveManager.GetManagerInstance().AcquirePSMove(0);
        moveCtrl[1] = PSMoveManager.GetManagerInstance().AcquirePSMove(1);

        for (int i = 0; i < moveCtrl.Length; i++)
        {
            if (moveCtrl[i].GetButtonSquare())
            {
                Debug.Log("Square pressed in psMove " + i);
            }
        }
        
        
	}
}
