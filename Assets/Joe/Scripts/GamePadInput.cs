﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePadInput : MonoBehaviour {

    /// <summary>
    /// (Property) The maximum speed which the player moves.
    /// </summary>
    public float maxSpeed = 2f;

    /// <summary>
    /// (Property) The acceleration of the player.
    /// </summary>
    public float acceleration = 0.02f;
    [Range(0f,1.0f)]
    public float inAirSlowing = 0.2f;

    /// <summary>
    /// (Field) Rigid body of player.
    /// </summary>
    public Rigidbody rigidBody;

    /// <summary>
    /// (Property) Players downward direction.
    /// </summary>
    public Vector3 startingGravitationalDirection;
    private Vector3 gravitationalDirection;

    /// <summary>
    /// (Property) Acceleration due to gravity.
    /// </summary>
    public float gravity = 0.001f;

    /// <summary>
    /// (Field) Booleans determining state.
    /// </summary>
    private bool inAir = true;
    private bool inCollider = true;
    private bool rotated = false;
    public GameObject coll = null;

    /// <summary>
    /// (Property) Player jump force.
    /// </summary>
    public float jumpForce = 10f;

    /// <summary>
    /// (Property) Speend of rotation.
    /// </summary>
    public float rotationalVelocity = 8f;

    /// <summary>
    /// (Property) Ray cast difference when in the air.
    /// </summary>
    public float rayDist = 0.1f;

    public Material outlineMaterial;

    /// <summary>
    /// Used to reset rotation.
    /// </summary>
    public void ResetRotation()
    {
        if (startingGravitationalDirection == Vector3.zero)
            gravitationalDirection = -transform.parent.up;
        else
            gravitationalDirection = startingGravitationalDirection;
    }

    /// <summary>
    /// Reset rotation of shape on start.
    /// </summary>
    void Start()
    {
        ResetRotation();
        rigidBody = transform.GetComponent<Rigidbody>();
    }

    /// <summary>
    /// Updating movement based on gravity and input
    /// </summary>
    void Update () {

        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");

        Vector3 forwardVector = Vector3.zero;
        Vector3 rightVector = Vector3.zero;

        float acc = 0;

        if (inAir)
        {
            forwardVector = Vector3.forward;
            rightVector = Vector3.right;
            acc = acceleration * inAirSlowing;
        }
        else
        {
            handleControls(inputX, inputY, ref forwardVector, ref rightVector);
            acc = acceleration;
        }

        Vector3 totalForce = inputX * rightVector.normalized + inputY * forwardVector.normalized;

        rigidBody.AddForce(totalForce.normalized * acc, ForceMode.VelocityChange);

        if (rigidBody.velocity.magnitude > maxSpeed && !inAir)
            rigidBody.velocity = maxSpeed * rigidBody.velocity.normalized;

        Vector3 jump = Vector3.zero;
        if (Input.GetButtonDown("Jump") && !inAir)
        {
            jump = -gravitationalDirection * jumpForce;
            inAir = true;
            rotated = false;
            gravitationalDirection = Vector3.down;
        }

        rigidBody.AddForce(jump, ForceMode.Impulse);
        rigidBody.AddForce(gravitationalDirection.normalized * gravity, ForceMode.Force);

        if (inAir)
        {
            gravitationalDirection = Vector3.down;
            rayCastLanding(transform.position, -transform.up, rayDist);
        }

        if (!rotated)
            rotateCharacter(gravitationalDirection);
    }


    /// <summary>
    /// Used for handling the controls of the player based on the cubes orientation.
    /// </summary>
    /// <param name="inputX">X controller input</param>
    /// <param name="inputY">Y controller input</param>
    /// <param name="forwardVector">Vector that will map onto X.</param>
    /// <param name="rightVector">Vector that will map onto Y.</param>
    private void handleControls(float inputX, float inputY, ref Vector3 forwardVector, ref Vector3 rightVector) {

        if (Mathf.Abs(Vector3.Angle(-gravitationalDirection, Vector3.up)) < 45 || (Mathf.Abs(Vector3.Angle(-gravitationalDirection, Vector3.down)) < 45))
        {
            if ((Mathf.Abs(Vector3.Angle(-gravitationalDirection, Vector3.down)) < 45))
            {
                forwardVector = Vector3.Cross(Vector3.left, -gravitationalDirection);
                rightVector = Vector3.Cross(Vector3.forward, -gravitationalDirection);
            }
            else
            {
                forwardVector = -Vector3.Cross(-Vector3.right, -gravitationalDirection);
                rightVector = -Vector3.Cross(Vector3.forward, -gravitationalDirection);
            }
        }
        else if (Mathf.Abs(Vector3.Angle(-gravitationalDirection, Vector3.right)) <= 30 || (Mathf.Abs(Vector3.Angle(-gravitationalDirection, Vector3.left)) <= 30))
        {
            if (Mathf.Abs(Vector3.Angle(-gravitationalDirection, Vector3.right)) <= 30)
            {
                forwardVector = Vector3.Cross(Vector3.forward, -gravitationalDirection);
                rightVector = Vector3.Cross(Vector3.down, -gravitationalDirection);
            }
            else if (Mathf.Abs(Vector3.Angle(-gravitationalDirection, Vector3.left)) <= 30)
            {
                forwardVector = Vector3.Cross(Vector3.back, -gravitationalDirection);
                rightVector = Vector3.Cross(Vector3.down, -gravitationalDirection);
            }

        }
        else if (Mathf.Abs(Vector3.Angle(-gravitationalDirection, Vector3.back)) < 45 || (Mathf.Abs(Vector3.Angle(-gravitationalDirection, Vector3.forward)) < 45))
        {
            if (Mathf.Abs(Vector3.Angle(-gravitationalDirection, Vector3.forward)) < 45)
            {
                forwardVector = Vector3.Cross(Vector3.left, -gravitationalDirection);
                rightVector = Vector3.Cross(Vector3.up, -gravitationalDirection);
            }
            else
            {
                forwardVector = Vector3.Cross(Vector3.right, -gravitationalDirection);
                rightVector = Vector3.Cross(Vector3.down, -gravitationalDirection);
            }
        }
    }

    /// <summary>
    /// When within a gravity field;
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerStay(Collider collision)
    {
        if (collision.tag == "Gravity" && !inAir && collision.gameObject == coll)
        {
            gravitationalDirection = -collision.GetComponent<Collider>().transform.up.normalized;
        }
    }

    /// <summary>
    /// When entering a NEW gravity field.
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Gravity" && inAir)
        {
            gravitationalDirection = -other.GetComponent<Collider>().transform.up.normalized;
            coll = other.gameObject;
            rotateCharacter(gravitationalDirection);
            inCollider = true;
            inAir = false;
            rotated = false;
        }

        if (other.tag == "Interactable")
        {
            Debug.Log("Collision");
            Renderer r = other.transform.GetComponent<Renderer>();
            r.material = outlineMaterial;
            other.transform.GetComponent<Rigidbody>().isKinematic = false;
        }
    }

    /// <summary>
    /// When exiting an EXISTING gravity field
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == coll)
        {
            inCollider = false;
            coll = null;
        }
    }

    /// <summary>
    /// When colliding with the floor.
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionStay(Collision collision)
    {
        inAir = false;
    }

    /// <summary>
    /// When leaving the floor.
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionExit(Collision collision)
    {
        inAir = true;
        rotated = false;
    }

    /// <summary>
    /// Uses the players local down and the gravitational fields vector to rotate to the correct orientation.
    /// </summary>
    /// <param name="down"></param>
    private void rotateCharacter (Vector3 down)
    {
        if (Vector3.Angle(-transform.up, down) < 10)
        { rotated = true; return; }

        //create the rotation we need to be in to look at the target
        Quaternion _lookRotation = Quaternion.LookRotation((-transform.up) - down);

        //rotate us over time according to speed until we are in the required rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, _lookRotation, Time.deltaTime * rotationalVelocity);
    }

    /// <summary>
    /// Checks if player is heading towards a field in the air based on velocity.
    /// </summary>
    /// <param name="pos">Origin</param>
    /// <param name="down">Local down</param>
    /// <param name="dist">Desired distance</param>
    private void rayCastLanding(Vector3 pos, Vector3 down, float dist)
    {
        RaycastHit hit;

        Debug.DrawRay(pos, rigidBody.velocity.normalized, Color.cyan);

        if (Physics.Raycast(pos, rigidBody.velocity.normalized, out hit, dist))
        {
            Debug.Log("Found");
            if (hit.collider.tag == "Gravity")
            {
                rotateCharacter(-hit.collider.transform.up);
            }
        }
    }

}
