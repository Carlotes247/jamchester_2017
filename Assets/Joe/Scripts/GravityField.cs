﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GravityField : MonoBehaviour
{
    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position, -transform.up);
    }
}
