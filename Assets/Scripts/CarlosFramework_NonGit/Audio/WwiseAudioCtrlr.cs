﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WwiseAudioCtrlr : MonoBehaviour {

    public GameObject audioObject;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update ()
    {

    }

    /// <summary>
    /// Sets the state of the tier switch
    /// </summary>
    /// <param name="stateToSwitch"></param>
    public void SetTierState(string stateToSwitch)
    {
        AkSoundEngine.SetSwitch("Tiers", stateToSwitch, audioObject);
        Debug.Log("Song switched to: " + stateToSwitch);
    }


    public void PlayEvent(string eventName, GameObject objectWithAudio)
    {
        AkSoundEngine.PostEvent(eventName, objectWithAudio);
        Debug.Log("Played SFX Event: " + eventName);

    }
}
