﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Serialization;


/// <summary>
/// Module for the PSMove to work with the EventSystem
/// </summary>
[AddComponentMenu("CarlosFramework/PSMoveInputModule")]
public class PSMoveInputModule : PointerInputModule
{
    private Vector2 m_LastPSMovePosition;
    private Vector2 m_PSMovePosition;

    [SerializeField]
    private float m_InputActionsPerSecond = 10;

    /// <summary>
    /// The delay in the time the input interpreted (we wait x seconds between frames)
    /// </summary>
    [SerializeField]
    private float m_RepeatDelay = 0.5f;

    /// <summary>
    /// (Field) Forces the module to be active if true
    /// </summary>
    [SerializeField]
    private bool m_ForceModuleActive;

    /// <summary>
    /// (Property) Forces the module to be active if true
    /// </summary>
    public bool forceModuleActive
    {
        get { return m_ForceModuleActive; }
        set { m_ForceModuleActive = value; }
    }

    public float inputActionsPerSecond
    {
        get { return m_InputActionsPerSecond; }
        set { m_InputActionsPerSecond = value; }
    }

    public float repeatDelay
    {
        get { return m_RepeatDelay; }
        set { m_RepeatDelay = value; }
    }

    /// <summary>
    /// The pointer class to work with
    /// </summary>
    private PointerEventData PSMovePointer;

    /// <summary>
    /// The psmove to run the logic on (always the 1st one)
    /// </summary>
    private PSMoveController PSMoveToCheck { get { return Toolbox.Instance.GameManager.InputController.Moves[0]; } }

    /// <summary>
    /// Update the internal state of the Module. Called on all modules before process is sent to the one activated module.
    /// (Called even if the module should not be active)
    /// </summary>
    public override void UpdateModule()
    {
        // We populate the variables of the PSMove this frame
        //Toolbox.Instance.GameManager.InputController.WiimoteInput.WiimoteInputLogic();

        m_LastPSMovePosition = m_PSMovePosition;
        m_PSMovePosition = Toolbox.Instance.GameManager.InputController.ScreenPointerPos;
    }

    /// <summary>
    /// If any of the conditions is true, the module is supported in the game
    /// </summary>
    /// <returns> True if any of the conditions is true</returns>
    public override bool IsModuleSupported()
    {
        /* If the module is force to be active 
        OR the mouse is present AND InputManager.InputType is PSMove
        returns true
        */
        return m_ForceModuleActive || Input.mousePresent && Toolbox.Instance.GameManager.InputController.InputType == InputController.TypeOfInput.PSMove;
    }

    /// <summary>
    /// If the module should be activated (Apparently, the function only executes if the Module is active)
    /// </summary>
    /// <returns></returns>
    public override bool ShouldActivateModule()
    {

        // If ShouldActivateModule is false in the base, we return false
        if (!base.ShouldActivateModule())
            return false;

        // We populate the variables of the wiimote this frame
        //Toolbox.Instance.GameManager.InputController.WiimoteInput.WiimoteInputLogic();

        // We save the value of ForceModuleActive in shouldActivate 
        var shouldActivate = m_ForceModuleActive;

        // |= is the OR assignment operator ( x |= y ) is ( x = x | y )
        // If the position of the PSMove has moved, we save true in shouldActivate, if not the value of m_ForceModuleActive stays
        shouldActivate |= (m_PSMovePosition - m_LastPSMovePosition).sqrMagnitude > m_RepeatDelay;
        // If the trigger of the PSMove has been pressed, we save true in shouldActivate, if not the value of m_ForceModuleActive stays
        shouldActivate |= ( (Toolbox.Instance.GameManager.InputController.GetTriggerPressedDown(PSMoveToCheck.TriggerValue, 0.5f)));
        //shouldActivate |= Toolbox.Instance.GameManager.InputController.ShootCommand.Called;

        // We return whatever the value is from the checks in the function
        return shouldActivate;
    }

    /// <summary>
    /// Called when the module is activated. Override this if you want custom code to execute when you activate your module
    /// </summary>
    public override void ActivateModule()
    {
        // We populate the variables of the wiimote this frame
        //Toolbox.Instance.GameManager.InputController.WiimoteInput.WiimoteInputLogic();

        base.ActivateModule();
        m_PSMovePosition = Toolbox.Instance.GameManager.InputController.ScreenPointerPos;
        m_LastPSMovePosition = Toolbox.Instance.GameManager.InputController.ScreenPointerPos;

        var toSelect = eventSystem.currentSelectedGameObject;
        if (toSelect == null)
            toSelect = eventSystem.firstSelectedGameObject;

        eventSystem.SetSelectedGameObject(toSelect, GetBaseEventData());

        // Get the PSMove controller
    }

    /// <summary>
    /// Called when the module is deactivated. Override this if you want custom code to execute when you deactivate your module.
    /// </summary>
    public override void DeactivateModule()
    {
        base.DeactivateModule();
        ClearSelection();
    }

    // Process is called once per tick
    public override void Process()
    {
        bool usedEvent = SendUpdateEventToSelectedObject();

        if (eventSystem.sendNavigationEvents)
        {
            if (!usedEvent)
            {
                SendSubmitEventToSelectedObject();
            }
        }
        ProcessPSMoveEvent();
    }

    /// <summary>
    /// Process submit keys
    /// </summary>
    /// <returns> Returns true if submit or cancel were received by selected object</returns>
    private bool SendSubmitEventToSelectedObject()
    {
        if (eventSystem.currentSelectedGameObject == null)
            return false;

        var data = GetBaseEventData();
        if (Toolbox.Instance.GameManager.InputController.GetTriggerPressedDown(PSMoveToCheck.TriggerValue, 0.5f))
            //if (Toolbox.Instance.GameManager.InputController.ShootCommand.Called)
            ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.submitHandler);

        //if (Input.GetButtonDown(m_CancelButton))
        //    ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.cancelHandler);
        return data.used;
    }

    /// <summary>
    /// See if the selected object has used the event
    /// </summary>
    /// <returns>Return true if it used the event</returns>
    private bool SendUpdateEventToSelectedObject()
    {
        if (eventSystem.currentSelectedGameObject == null)
        {
            return false;
        }

        // Generates a BaseEventData that can be used by the event system
        var data = GetBaseEventData();
        ExecuteEvents.Execute(eventSystem.currentSelectedGameObject, data, ExecuteEvents.updateSelectedHandler);
        // return if the event is used
        return data.used;
    }


    private void ProcessPSMoveEvent()
    {
        // Getting objects related to player
        GetPointerData(0, out PSMovePointer, true);

        // We update the pointer in screen coords
        //wiimotePointer.position = Toolbox.Instance.GameManager.InputController.WiimoteInput.CursorPosition;ç
        PSMovePointer.position = m_PSMovePosition;

        // Raycasting
        eventSystem.RaycastAll(PSMovePointer, this.m_RaycastResultCache);
        RaycastResult raycastResult = FindFirstRaycast(this.m_RaycastResultCache);
        PSMovePointer.pointerCurrentRaycast = raycastResult;
        // We process the press of the pointer (PointerDown, PointerUp)
        this.ProcessPSMovePress(PSMovePointer);
        // We process the move of the pointer
        this.ProcessMove(PSMovePointer);
        // We process the drag of the pointer
        this.ProcessDrag(PSMovePointer);


        PSMovePointer.clickCount = 0;
        // If cursor click...
        if (Toolbox.Instance.GameManager.InputController.GetTriggerPressedDown(PSMoveToCheck.TriggerValue, 0.5f))
        //if (Toolbox.Instance.GameManager.InputController.ShootCommand.Called)
        {
            // Click
            PSMovePointer.pressPosition = Toolbox.Instance.GameManager.InputController.ScreenPointerPos;
            PSMovePointer.clickTime = Time.unscaledTime;
            PSMovePointer.pointerPressRaycast = raycastResult;

            PSMovePointer.clickCount = 1;
            PSMovePointer.eligibleForClick = true;

            // Dragging
            PSMovePointer.dragging = false;
            PSMovePointer.useDragThreshold = false;

            // If there are anyobjects in front of the cursor (the ray hitted something)
            if (this.m_RaycastResultCache.Count > 0)
            {
                PSMovePointer.selectedObject = raycastResult.gameObject;

                // search for the control that will receive the press
                // if we can't find a press handler set the press
                // handler to be what would receive a click.
                var newPressed = ExecuteEvents.ExecuteHierarchy(PSMovePointer.selectedObject, PSMovePointer, ExecuteEvents.pointerDownHandler);
                // didnt find a press handler... search for a click handler
                if (newPressed == null)
                {
                    newPressed = ExecuteEvents.GetEventHandler<IPointerClickHandler>(PSMovePointer.selectedObject);
                }

                //wiimotePointer.pointerPress = ExecuteEvents.ExecuteHierarchy(raycastResult.gameObject, wiimotePointer, ExecuteEvents.submitHandler);
                PSMovePointer.pointerPress = newPressed;
                PSMovePointer.rawPointerPress = raycastResult.gameObject;

                PSMovePointer.clickTime = Time.unscaledTime;

                // save the drag handler as well
                PSMovePointer.pointerDrag = ExecuteEvents.GetEventHandler<IDragHandler>(PSMovePointer.selectedObject);

                if (PSMovePointer.pointerDrag != null)
                {
                    ExecuteEvents.Execute(PSMovePointer.pointerDrag, PSMovePointer, ExecuteEvents.initializePotentialDrag);
                }
            }
            else
            {
                PSMovePointer.selectedObject = null;
                PSMovePointer.pointerPress = null;
                PSMovePointer.rawPointerPress = null;
            }
        }
        // If not cursor click...
        else
        {
            PSMovePointer.clickCount = 0;
            PSMovePointer.eligibleForClick = false;
            PSMovePointer.pointerPress = null;
            PSMovePointer.rawPointerPress = null;
        }

    }

    /// <summary>
    /// Process the current PSMovePress (PointerDown, PointerUp)
    /// </summary>
    /// <param name="data"> The PSMovePointer to pass in </param>
    protected void ProcessPSMovePress(PointerEventData data)
    {
        // We already have the buttonData
        var pointerEvent = data;
        var currentOverGo = pointerEvent.pointerCurrentRaycast.gameObject;

        // PointerDown notification (pressed this frame)
        if (Toolbox.Instance.GameManager.InputController.GetTriggerPressedDown(PSMoveToCheck.TriggerValue, 0.5f))
        //if (Toolbox.Instance.GameManager.InputController.ShootCommand.Called)
        {
            
            pointerEvent.eligibleForClick = true;
            pointerEvent.delta = Vector2.zero;
            pointerEvent.dragging = false;
            pointerEvent.useDragThreshold = true;
            pointerEvent.pressPosition = pointerEvent.position;
            pointerEvent.pointerPressRaycast = pointerEvent.pointerCurrentRaycast;

            DeselectIfSelectionChanged(currentOverGo, pointerEvent);

            // search for the control that will receive the press
            // if we can't find a press handler set the press
            // handler to be what would receive a click.
            var newPressed = ExecuteEvents.ExecuteHierarchy(currentOverGo, pointerEvent, ExecuteEvents.pointerDownHandler);

            // didnt find a press handler... search for a click handler
            if (newPressed == null)
                newPressed = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentOverGo);

            // Debug.Log("Pressed: " + newPressed);

            float time = Time.unscaledTime;

            if (newPressed == pointerEvent.lastPress)
            {
                var diffTime = time - pointerEvent.clickTime;
                if (diffTime < m_RepeatDelay)
                    ++pointerEvent.clickCount;
                else
                    pointerEvent.clickCount = 1;

                pointerEvent.clickTime = time;
            }
            else
            {
                pointerEvent.clickCount = 1;
            }

            pointerEvent.pointerPress = newPressed;
            pointerEvent.rawPointerPress = currentOverGo;

            pointerEvent.clickTime = time;

            // Save the drag handler as well
            pointerEvent.pointerDrag = ExecuteEvents.GetEventHandler<IDragHandler>(currentOverGo);

            if (pointerEvent.pointerDrag != null)
                ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.initializePotentialDrag);
        }

        // PointerUp notification (NOT pressed this frame)
        if (!(Toolbox.Instance.GameManager.InputController.GetTriggerPressedDown(PSMoveToCheck.TriggerValue, 0.5f)))
        //if (!Toolbox.Instance.GameManager.InputController.ShootCommand.Called)
        {
            // Debug.Log("Executing pressup on: " + pointer.pointerPress);
            ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerUpHandler);

            // Debug.Log("KeyCode: " + pointer.eventData.keyCode);

            // see if we mouse up on the same element that we clicked on...
            var pointerUpHandler = ExecuteEvents.GetEventHandler<IPointerClickHandler>(currentOverGo);

            // PointerClick and Drop events
            if (pointerEvent.pointerPress == pointerUpHandler && pointerEvent.eligibleForClick)
            {
                ExecuteEvents.Execute(pointerEvent.pointerPress, pointerEvent, ExecuteEvents.pointerClickHandler);
            }
            else if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
            {
                ExecuteEvents.ExecuteHierarchy(currentOverGo, pointerEvent, ExecuteEvents.dropHandler);
            }

            pointerEvent.eligibleForClick = false;
            pointerEvent.pointerPress = null;
            pointerEvent.rawPointerPress = null;

            if (pointerEvent.pointerDrag != null && pointerEvent.dragging)
                ExecuteEvents.Execute(pointerEvent.pointerDrag, pointerEvent, ExecuteEvents.endDragHandler);

            pointerEvent.dragging = false;
            pointerEvent.pointerDrag = null;

            // redo pointer enter / exit to refresh state
            // so that if we moused over somethign that ignored it before
            // due to having pressed on something else
            // it now gets it.
            if (currentOverGo != pointerEvent.pointerEnter)
            {
                HandlePointerExitAndEnter(pointerEvent, null);
                HandlePointerExitAndEnter(pointerEvent, currentOverGo);
            }
        }
    }
}
