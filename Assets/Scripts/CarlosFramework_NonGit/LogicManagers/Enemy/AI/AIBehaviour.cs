﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

///<summary>
/// This is the base script for any AIBehaviour. It needs to be inherited by other AIBehaviours
///</summary>
public abstract class AIBehaviour : MonoBehaviour {

    /// <summary>
    /// The basic AIBehaviour function - each derived class knows waht to do with this function
    /// </summary>
    public abstract void Behave();

}
