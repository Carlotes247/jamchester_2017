﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// AIDynamic, move predictably between points
/// </summary>
[AddComponentMenu("CarlosFramework/AIDynamicBehaviour")]
public class AIDynamicBehaviour : AIBehaviour
{

    #region Fields&Attributes
    /// The Enemy Manager so that this script has acces to the rest of components
    // Field
    [SerializeField]
    private ObjectManager objectManager;
    // Property
    public ObjectManager ObjectManager { get { return this.objectManager; } set { this.objectManager = value; } }

    /// The selection mode of points enumaeration
    public enum PointSelectionModeEnum
    {
        Normal,
        Inverse,
        Random
    }
    [SerializeField]
    private PointSelectionModeEnum pointSelectionMode;
    /// <summary>
    /// (Property) The mode in which the points will be selected for moving
    /// </summary>
    public PointSelectionModeEnum PointSelectionMode { get { return this.pointSelectionMode; } set { this.pointSelectionMode = value; } }

    /// <summary>
    /// (Field) The index of the array of points to go
    /// </summary>
    [SerializeField]
    private int indexOfPointsToGo;
    /// <summary>
    /// (Property) The index of the array of points to go
    /// </summary>
    public int IndexOfPointsToGo { get { return this.indexOfPointsToGo; } set { this.indexOfPointsToGo = value; } } 
    #endregion

    /// <summary>
    /// The Dynamic Behaviour will move the object to as many points as are specified in the AIController
    /// </summary>
    public override void Behave()
    {
        // We make the object go to a new point only when it has reached a point or is not moving at all
        if (ObjectManager.MovementController.MovementPhase == MovementController.MovementPhaseEnum.Stopped)
        {
            SwitchPoint(1);
        }
        
        // We move the object to the point
        //ObjectManager.MovementController.Move(ObjectManager.AIController.PointsToGo[IndexOfPointsToGo]);
        ObjectManager.MovementController.Move(ObjectManager.PointsController.PointsToGo[IndexOfPointsToGo]);

        /* DEPRECATED. Now the animation is controlled in the movement controller */
        //// We animate the object to show the movement
        //ObjectManager.AnimController.Walk(true);
    }

    /// <summary>
    /// Changes the index to access the array of pointsToGo in the AIController by the specified amount
    /// </summary>
    /// <param name="offSet"> The amount to add to the integer </param>
    private void SwitchPoint (int offSet)
    {
        
        int aux = 0;

        switch (PointSelectionMode)
        {
            case PointSelectionModeEnum.Normal:
                aux = IndexOfPointsToGo + offSet;
                // If it is higher than the length of the array, we reset the index to 0
                if (aux > ObjectManager.PointsController.PointsToGo.Length - 1)
                {
                    aux = 0;
                }
               
                break;
            case PointSelectionModeEnum.Inverse:
                aux = IndexOfPointsToGo;
                // We update the value of the index if it is still above 0
                if (aux > 0)
                {
                    aux -= offSet;                    
                }
                // If it is lower than 0, we set the index to the length of the array minus 1 (the maximun index possible)
                else
                {
                    aux = objectManager.PointsController.PointsToGo.Length - 1;
                }                
                                                                
                break;
            case PointSelectionModeEnum.Random:
                aux = UnityEngine.Random.Range(0, ObjectManager.PointsController.PointsToGo.Length);
                break;
            default:
                break;
        }

        // We set the desired index
        IndexOfPointsToGo = aux;     

    }

    

}
