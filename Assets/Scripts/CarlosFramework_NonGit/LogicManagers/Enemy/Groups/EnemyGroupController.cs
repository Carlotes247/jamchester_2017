﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using ReusableMethods;
using System.Collections.Generic;
using System;

[AddComponentMenu("CarlosFramework/EnemyGroupController")]
public class EnemyGroupController : MonoBehaviour
{

    /// <summary>
    /// (Field) The enemies in this group
    /// </summary>
    [SerializeField]
    private GameObject[] m_EnemiesInGroup;

    /// <summary>
    /// (Field) Flag that controls if the group was defeated already
    /// </summary>
    [SerializeField]
    private bool m_GroupDefeated;
    /// <summary>
    /// (Property) Flag that controls if the group was defeated already
    /// </summary>
    public bool GroupDefeated { get { return this.m_GroupDefeated; } }

    /// <summary>
    /// (Field) Flag to know if the group was spawned already
    /// </summary>
    private bool m_GroupSpawned;

    /// <summary>
    /// (Field) Counter of enemies defeated in the group
    /// </summary>
    private int m_NumberEnemiesDefeated;

    // This function is called when the object becomes enabled and active
    public void OnEnable()
    {
        Initialize();
    }
    
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Initializes the component to its default values
    /// </summary>
    private void Initialize()
    {
        // We haven't spawned nothing
        m_GroupSpawned = false;
        // The group is not defeated
        m_GroupDefeated = false;
        // No one is defeated (yet)
        m_NumberEnemiesDefeated = 0;

        // If the array of enemies of the current component is empty or null...
        if (m_EnemiesInGroup == null || m_EnemiesInGroup.Length < 1)
        {            
            // We find all the enemies children to the current object
            FindEnemiesAsChildren();
        }

        // We set all enemies to be deactivated
        Arrays.SetActiveAllArray<GameObject>(ref m_EnemiesInGroup, false);
    }

    /// <summary>
    /// Finds enemies as children to the current object
    /// </summary>
    private void FindEnemiesAsChildren()
    {
        //// We initialize a new temporary list of enemies
        //List<EnemyManager> EnemyList = new List<EnemyManager>();
        //// We fill the list from the children of the current object
        //this.GetComponentsInChildren<EnemyManager>(true, EnemyList);
        //// If we found any...
        //if (EnemyList.Count > 0)
        //{
        //    // We then copy this list to our component field 
        //    // We resize the array 
        //    Array.Resize<GameObject>(ref m_EnemiesInGroup, EnemyList.Count);
        //    // We copy each result as a gameObject to the component array
        //    for (int i = 0; i < EnemyList.Count; i++)
        //    {
        //        m_EnemiesInGroup[i] = EnemyList[i].gameObject;
        //    }
        //}
        Arrays.FindComponentAsChildren<EnemyManager>(ref m_EnemiesInGroup, this.gameObject);
    }

    /// <summary>
    /// Spawns the whole group of enemies
    /// </summary>
    public void SpawnGroup()
    {
        // If the group was not spawned already...
        if (!m_GroupSpawned)
        {
            // We activate the whole group of enemies
            Arrays.SetActiveAllArray<GameObject>(ref m_EnemiesInGroup, true);
            // We mark the the component as spawned already
            m_GroupSpawned = true;

            Debug.Log("SpawnGroup Called!");
        }
    }

    /// <summary>
    /// Method called when an enemy is defeated to keep track of how many enemies are defeated
    /// </summary>
    public void EnemyDefeated()
    {
        // We increase the number of enemies defeated
        m_NumberEnemiesDefeated++;
        // If the number of enemies defeated is equal or higher than the number of enemies in the group...
        if (m_NumberEnemiesDefeated >= m_EnemiesInGroup.Length)
        {
            // We set the flag of group defeated to true
            m_GroupDefeated = true;
        }
    }

    /// <summary>
    /// Checks if the passed in enemyGroupCtrl is defeated or not
    /// </summary>
    /// <param name="enemyGroupCtrl"> The EnemyGroupController to check</param>
    /// <returns> True if defeated, False if not</returns>
    public static bool CheckEnemyGroupDefeated(EnemyGroupController enemyGroupCtrl)
    {

        return enemyGroupCtrl.GroupDefeated;
    }
}
