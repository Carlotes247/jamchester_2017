﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A command that is completely empty
/// </summary>
public class EmptyCommand : InputCommand
{

    /// <summary>
	/// (Field) Flag that indicate if the method was called
	/// </summary>
	private bool m_Called;
    /// <summary>
    /// (Property) Flag that indicate if the method was called
    /// </summary>
    public override bool Called
    {
        get { return this.m_Called; }
        set { this.m_Called = value; }
    }

    /// <summary>
    /// (Field) Flag that indicates if the execution was succesful
    /// </summary>
    private bool m_Success;
    /// <summary>
    /// (Property) Flag that indicates if the execution was succesful
    /// </summary>
    public override bool Success
    {
        get { return this.m_Success; }

        set { this.m_Success = value; }
    }

    
    /// <summary>
    /// Executes the DashCommand
    /// </summary>
    /// <param name="actor"> The actor that will perform the dash</param>
    public override void Execute(ObjectManager actor)
    {

        // We update the Called flag to let other classes know that Execute was called
        m_Called = true;
    }

    // LateUpdate is called every frame, if the Behaviour is enabled
    public void LateUpdate()
    {
        // After all the calculations have been performed, we force the update of the called flag to false
        m_Called = false;
    }
}
