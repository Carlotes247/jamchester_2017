﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

/// <summary>
/// Input Command provides a base class that represents a triggerable game command
/// </summary>
public class InputCommand {

    /// <summary>
    /// (Property) Flag that indicates if the command was called. Read Only
    /// </summary>
    public virtual bool Called { get; set; }

    /// <summary>
    /// (Property) Flag that indicates if the command was succesful in the execution
    /// </summary>
    public virtual bool Success { get; set; }

    /// <summary>
    /// Executes the command 
    /// </summary>
    /// <param name="actor"> The actor that will execute it</param>
    public virtual void Execute (ObjectManager actor) { }

	/// <summary>
	/// Executes the command, with one vector3 parameters
	/// </summary>
	/// <param name="actor">The actor that will execute it</param>
	/// <param name="var1"> First vector3 parameter</param>
	public virtual void Execute (ObjectManager actor, Vector3 var1) { }

    /// <summary>
    /// Executes the command, with two vector3 parameters
    /// </summary>
    /// <param name="actor">The actor that will execute it</param>
    /// <param name="var1"> First vector3 parameter</param>
    /// <param name="var2"> Second vector3 parameter</param>
    public virtual void Execute (ObjectManager actor, Vector3 var1, Vector3 var2) { }
}
