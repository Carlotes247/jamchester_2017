﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// The CameraManger holds references to important components of the MainCamera
/// </summary>
[AddComponentMenu("CarlosFramework/CameraManager")]
public class CameraManager : ObjectManager
{
    #region Fields&Properties

    /// <summary>
    /// (Field) The Camera Component of the current object
    /// </summary>
    [SerializeField]
    private Camera m_CameraComponent;
    /// <summary>
    /// (Property) The Camera Component of the current object
    /// </summary>
    public Camera CameraComponent { get { return this.m_CameraComponent; } }

    /// <summary>
    /// (Field) The RotationController of the current object
    /// </summary>
    [SerializeField]
    private RotationController m_RotationController;
    /// <summary>
    /// (Property) The RotationController of the current object
    /// </summary>
    public override RotationController RotationController { get { return this.m_RotationController; } }

    /// <summary>
    /// (Field) The Transform component of the current object
    /// </summary>
    [SerializeField]
    private Transform m_ObjectTransform;
    /// <summary>
    /// (Property) The Transform component of the current object
    /// </summary>
    public override Transform ObjectTransform { get { return this.m_ObjectTransform; } }

    #region NotImplementedProperties
    public override Rigidbody ObjectRigidbody
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public override LifeController LifeController
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public override MovementController MovementController
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public override AIController AIController
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public override TimerController TimerController
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public override PointsController PointsController
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public override AnimController AnimController
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public override WeaponController WeaponController
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public override SightController SightController
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public override bool AllowGizmos
    {
        get
        {
            throw new NotImplementedException();
        }
    }
    #endregion

    #endregion
    // Awake is called when the script instance is being loaded
    public void Awake()
    {
        // We get references to the components we are referencing
        m_CameraComponent = this.gameObject.GetComponent<Camera>();
        m_RotationController = this.gameObject.GetComponent<RotationController>();
        m_ObjectTransform = this.gameObject.GetComponent<Transform>();
    }

    
}
