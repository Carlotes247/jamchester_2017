﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;

/// <summary>
/// This controller will make the current object lookAt an objective using CarlosFrameWork for fast-prototyping
/// </summary>
[AddComponentMenu("CarlosFramework/LookAtController")]
public class LookAtController : MonoBehaviour
{

    /// <summary>
    /// (Field) The objective we want to look at
    /// </summary>
    [SerializeField]
    private Transform m_ObjectiveToLookAt;    

    // Awake is called when the script instance is being loaded
    public void Awake()
    {
        
    }
    
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // We look at the main camera
        this.transform.LookAt(m_ObjectiveToLookAt);
    }
}
