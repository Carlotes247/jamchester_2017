﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using System;
using ReusableMethods;

[AddComponentMenu("CarlosFramework/GameLevelLogic")]
public class GameLevelLogic : MonoBehaviour
{
    [SerializeField]
    public bool m_AllowDebug;

    /// <summary>
    /// The initial position of the player in the level
    /// </summary>
    [SerializeField]
    private Vector3 m_PlayerInitialPosInLevel;

	/// <summary>
	/// The initial positions of the players in case there is more than one
	/// </summary>
	[SerializeField]
	private Vector3[] m_PlayersInitialPosInLevel;

    /// <summary>
    /// (Field) True if the level is won
    /// </summary>
    [SerializeField]
    private bool m_LevelWinFlag;
    /// <summary>
    /// (Property) True if the game is won
    /// </summary>
    public bool LevelWinFlag { get { return this.m_LevelWinFlag; } }

    public bool DontRestartWinFlag;

    /// <summary>
    /// (Field) The text to show when the level loads for the first time
    /// </summary>
    [SerializeField]
    private string m_FirstText;


    /// <summary>
    /// (Field) Array containing all the groups of enemies
    /// </summary>
    [SerializeField]
    private EnemyGroupController[] m_EnemyGroups;
    /// <summary>
    /// (Field) Will contain how many groups of enemies have been defeated already
    /// </summary>
    private bool[] m_EnemyGroupsDefeated;

	/// <summary>
	/// The player that won the level (we refer to playerID)
	/// </summary>
	[SerializeField]
	private int m_PlayerThatWonLevel;
	/// <summary>
	/// The player that won the level (we refer to playerID)
	/// </summary>
	/// <value>The player that won level.</value>
	public int PlayerThatWonLevel { get { return m_PlayerThatWonLevel; } set { this.m_PlayerThatWonLevel = value; } }

    /// <summary>
    /// The end level text
    /// </summary>
    [SerializeField]
    private string m_EndLevelText;

    /// <summary>
    /// Song to play when we finish the level
    /// </summary>
    [SerializeField, Tooltip("Song to play when we finish the level")]
    private string m_SongToPlayNext;


    // This function is called when the object becomes enabled and active
    public void OnEnable()
    {
        // We initialize the game
        Initialize();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Initializes the level to its default values
    /// </summary>
    private void Initialize()
    {
        // We set the player in the initial position of the level
        //SetPlayerAtInitialPos();

        // We show the objective in the HUD
        try
        {
            Toolbox.Instance.GameManager.HudController.UpdateStoryText(m_FirstText);
        }
        catch (Exception)
        {

            if (m_AllowDebug)
                Debug.LogError("UpdateStoryText returns null!");
        }


        if (!DontRestartWinFlag)
        {
            // We set all the flags to false
            m_LevelWinFlag = false;
        }

        // If the array of enemy groups is empty...
        if (m_EnemyGroups.Length <1 )
        {
            //... We find all the enemiyGroups in the level
            Arrays.FindComponentAsChildren<EnemyGroupController>(ref m_EnemyGroups, this.gameObject);
        }
    }

    /// <summary>
    /// Wins then level when called
    /// </summary>
    public void WinLevel()
    {
        if (!m_LevelWinFlag)
        {
            // If all the groups of enemies have been defeated...
            if (Array.TrueForAll(m_EnemyGroups, EnemyGroupController.CheckEnemyGroupDefeated))
            {
                // We set the win flag to true
                m_LevelWinFlag = true;
                if (m_AllowDebug)
                {
                    Debug.Log("Level Won");
                }
                Toolbox.Instance.GameManager.HudController.UpdateStoryText("Level Won");

                // We execute any logic that carry winning a level
                WinLevelLogic();

                Toolbox.Instance.GameManager.GameLevelController.LoadNextLevel();

                // We try and win the level (since we know the ID of who won)
                Toolbox.Instance.GameManager.GameLogicController.Win(PlayerThatWonLevel);

            }
            // If there are enemies left to defeat...
            else
            {
                if (m_AllowDebug)
                    Debug.Log("There are still enemies left to defeat!");
                Toolbox.Instance.GameManager.HudController.UpdateStoryText("There are still enemies left to defeat!");
            }
        }
    }

    /// <summary>
    /// The logic of winning a level
    /// </summary>
    private void WinLevelLogic() {
        // Show the end level UI
        Toolbox.Instance.GameManager.HudController.EndLevelUI.ShowUI(m_EndLevelText);

        // Play song specified
        Toolbox.Instance.GameManager.AudioController.PlayTier(m_SongToPlayNext);

        // Play win sound
        Toolbox.Instance.GameManager.AudioController.PlayEvent("Win", this.gameObject);
    }

	/// <summary>
	/// Restarts the state of the level (the level won't be won)
	/// </summary>
	public void RestartLevelState() {
        // We exit the method if we should not restart the win flag
        if (DontRestartWinFlag)
        {
            return;
        }
		m_LevelWinFlag = false;
	}

    /// <summary>
    /// Static method to check if a level is completed
    /// </summary>
    /// <param name="levelToCheck"> The GameObject containing the GameLevelLogic component to check</param>
    /// <returns> True if completed, False if not</returns>
    public static bool CheckLevelCompleted(GameObject levelToCheck)
    {
        // If the object passed in doesn't have the component we are looking for...
        if (levelToCheck.GetComponent<GameLevelLogic>() == null)
        {
            // ... We throw an exception
            throw new UnityException("The level " + levelToCheck.name + " doesn't have a GameLevelLogic component!");
        }

        if (levelToCheck.GetComponent<GameLevelLogic>().m_AllowDebug)
            Debug.Log( "Level flag" + levelToCheck.GetComponent<GameLevelLogic>().LevelWinFlag);

        // We return the WinFlag of the GameLevelLogic in that level
        return levelToCheck.GetComponent<GameLevelLogic>().LevelWinFlag;
    }

    /// <summary>
    /// Puts the player at the initial pos
    /// </summary>
    public void SetPlayerAtInitialPos()
    {
		if (Toolbox.Instance.GameManager.GameLogicController != null) {
			// If there is only one player...
			if (Toolbox.Instance.GameManager.Players.Length < 2) {
                // We set the player in the initial position of the level
                //Toolbox.Instance.GameManager.GameLogicController.SetPlayerAtInitialPosition(m_PlayerInitialPosInLevel);
                gameObject.GetComponentInChildren<RotationRoomCtrl>().SetPlayerAtInitialPos();
			}
			// If there are several players...
			else {
				// We set the players in their initial positions
				Toolbox.Instance.GameManager.GameLogicController.SetPlayersAtInitialPosition(m_PlayersInitialPosInLevel);
			}
		}
	}



    // Implement this OnDrawGizmosSelected if you want to draw gizmos only if the object is selected
    public void OnDrawGizmosSelected()
    {
		// If there is no multiplayer defined...
		if (m_PlayersInitialPosInLevel.Length < 1) {
			// Green color for initial Player Pos
			Gizmos.color = Color.green;
			// We draw a cube where the player will start
			Gizmos.DrawCube(m_PlayerInitialPosInLevel, Vector3.one);
		} else {
			for (int i = 0; i < m_PlayersInitialPosInLevel.Length; i++) {
				// switch the color for the gizmo
				switch (i) {
				case 0:
					// Green color for P0
					Gizmos.color = Color.green;
					break;
				case 1:
					// Red color for P1
					Gizmos.color = Color.red;
					break;
				case 2:
					// Blue color for P2
					Gizmos.color = Color.blue;
					break;
				case 3:
					// Yellow color for P3
					Gizmos.color = Color.yellow;
					break;
				default:
					break;
				}


				// We draw a cube where that player will start
				Gizmos.DrawSphere(m_PlayersInitialPosInLevel[i], 0.1f);
			}
		}

    }
}
