﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------

/*! This script will take charge of controlling the global score of the game */
using UnityEngine;
using System.Collections;

/// <summary>
/// The controller of the score, in charge of adding and removing points
/// </summary>
[AddComponentMenu("CarlosFramework/ScoreController")]
public class ScoreController : MonoBehaviour {

    /// The global gameScore for the game
    // Field
    private float gameScore;
    // Property
    public float GameScore { get { return this.gameScore; } set { this.gameScore = value; } }

	// Use this for initialization
	void Start () {
        GameScore = 0;
        Toolbox.Instance.GameManager.HudController.UpdateHUDScore(GameScore.ToString());
    }

    // Update is called once per frame
    void Update () {
        //Toolbox.Instance.GameManager.HudController.UpdateHUDScore(GameScore.ToString());
        //Toolbox.Instance.GameManager.HudController.TestAlive();
        
    }

    /// The function in charge of adding a certain amount of points to the global score
    void AddPoints (float amount)
    {
        GameScore += amount;
    }

    /// The public functions that updates the score
    public void UpdateScore (float amount)
    {
        AddPoints(amount);
        Toolbox.Instance.GameManager.HudController.UpdateHUDScore(GameScore.ToString());
    }

    public void TestAlive()
    {
        if (Toolbox.Instance.GameManager.AllowDebugCode)
        {
            Debug.Log("ScoreController Alive!!" + GameScore.ToString()); 
        }
    }

    public void DebugScore()
    {
        if (Toolbox.Instance.GameManager.AllowDebugCode)
        {
            Debug.Log("Score is: " + GameScore.ToString());
        }
    }
}
