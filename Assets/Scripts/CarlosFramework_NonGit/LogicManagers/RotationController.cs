﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------
using UnityEngine;
using System.Collections;
using ReusableMethods;

/// <summary>
/// Controls the code to rotate and lookAt of the current object
/// </summary>
[AddComponentMenu("CarlosFramework/RotationController")]
public class RotationController : MonoBehaviour
{

    /// <summary>
    /// (Field) The ObjectManager to access all of the components of this object
    /// </summary>
    [SerializeField]
    private ObjectManager m_ObjectManager;

    /// <summary>
    /// (Field) Flag to know if the controller is currently rotating something. 
    /// </summary>
    [SerializeField]
    private bool m_ControlRotation;

    /// <summary>
    /// (Field) The Initial Rotation of the object when it was first loaded
    /// </summary>
    private Vector3 m_InitialRotation;

    /// <summary>
    /// (Field) The rotation of the object in eulerAngles
    /// </summary>
    [SerializeField]
    private Vector3 m_RotationObject;
    /// <summary>
    /// (Property) The rotation of the object in eulerAngles
    /// </summary>
    public Vector3 RotationObject { get { return this.m_RotationObject; } set { this.m_RotationObject = value; } }

    /// <summary>
    /// (Field) The Axis of the object scalated to (-1, 1)
    /// </summary>
    [SerializeField]
    private Vector2 m_ObjectAxis;
    /// <summary>
    /// (Property) The Axis of the object scalated to (-1, 1)
    /// </summary>
    private Vector2 ObjectAxis { get { return m_ObjectAxis; } set { this.m_ObjectAxis = value; } }

    /// <summary>
    /// (Field) The Dead Zone of the object. If the axis are outside this zone, the object will rotate
    /// </summary>
    [SerializeField]
    private Vector2 m_ObjectDeadZoneAxis;

    /// <summary>
    /// (Field) The speed of the rotation of the object
    /// </summary>
    [SerializeField, Range(0f, 1f)]
    private float m_ObjectRotationSpeed;

    [SerializeField, Range(1f, 5f), Tooltip("Increases the speed by the specified factor")]
    private float m_RotationSpeedMultiplier;

    /// <summary>
    ///  The definition for the enum of rotation option
    /// </summary>
    private enum RotationOptionsEnum
    {
        All, xy, x, y, z
    }
    /// <summary>
    /// (Field) The options to rotate the object
    /// </summary>
    [SerializeField]
    private RotationOptionsEnum m_RotationOptions;

    // Awake is called when the script instance is being loaded
    public void Awake()
    {
        // We load references to the needed components
        m_ObjectManager = this.GetComponent<ObjectManager>();

        // We save the initial rotation of the object
        m_InitialRotation = m_ObjectManager.ObjectTransform.rotation.eulerAngles;
    }

    // This function is called when the object becomes enabled and active
    public void OnEnable()
    {
        // We initialize the component to its default values
        Initialize();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// Initializes the component to the default values
    /// </summary>
    private void Initialize()
    {
        // We set the object to its initial rotation
        RotateObject(m_InitialRotation, m_RotationOptions);
    }


    /// <summary>
    /// Public function to rotate the current object, using axis passed in
    /// </summary>
    /// <param name="axis"> The axis to rotate the object. Needs to be (-1, 1)</param>
    public void Rotate(Vector2 axis)
    {
        if (m_ControlRotation)
        {
            // We update the current object axis
            UpdateObjectAxis(axis);
            // We check if the axis are not in the deadZone
            m_ObjectAxis = CheckDeadZoneAxis(m_ObjectAxis, m_ObjectDeadZoneAxis);
            // We update the internal component rotation, before applying it
            UpdateInternalRotation(m_ObjectAxis, m_ObjectRotationSpeed);
            // We rotate the object
            RotateObject(m_RotationObject, m_RotationOptions);
        }

    }

    /// <summary>
    /// Rotates the object directly into the eulerAngles passed in
    /// </summary>
    /// <param name="eulerAnglesToRotate"> The eulerAngles to rotate</param>
    public void Rotate(Vector3 eulerAnglesToRotate)
    {
        // If the controller is working...
        if (m_ControlRotation)
        {
            // We rotate the object in all angles with the angles passed in
            RotateObject(eulerAnglesToRotate, RotationOptionsEnum.All);
        }
    }

    /// <summary>
    /// Updates the Object Axis to a value between (-1, 1)
    /// </summary>
    /// <param name="axisValues"> The axis values passed in. Needs to be in (-1, 1)</param>
    private void UpdateObjectAxis(Vector2 axisValues)
    {
        // X is the Scalated from the normalized screenPointerPos and the screen width
        m_ObjectAxis.x = axisValues.x;
        // Y is the Scalated from the normalized screenPointerPos and the screen height
        m_ObjectAxis.y = axisValues.y;

    }

    /// <summary>
    /// Updates the internal rotation values of the class
    /// </summary>
    /// <param name="axisValues"> The axisValues passed in. In (-1, 1)</param>
    /// <param name="rotationSpeed"> The rotationSpeed to which update.</param>
    private void UpdateInternalRotation(Vector2 axisValues, float rotationSpeed)
    {
        //m_YawCamera += 2f * ReusableMethods.Normalization.ScaleNormalize((ReusableMethods.Normalization.Normalize(ScreenPointerPos.x, 0, Screen.width)), -1, 1);
        //m_YawCamera += 2f * m_CameraAxis.x;
        m_RotationObject.y += rotationSpeed * axisValues.x * m_RotationSpeedMultiplier;
        //m_PitchCamera -= 2f * ReusableMethods.Normalization.ScaleNormalize((ReusableMethods.Normalization.Normalize(ScreenPointerPos.y, 0, Screen.height)), -1, 1);
        //m_PitchCamera -= 2f * m_CameraAxis.y;
        m_RotationObject.x -= rotationSpeed * axisValues.y * m_RotationSpeedMultiplier;

        // We limit the rotation in the Y axis to 90 degrees (that is the X one in euler angles)
        if (Mathf.Abs(m_RotationObject.x) > 90)
        {
            m_RotationObject.x = 90 * Mathf.Sign(m_RotationObject.x);
        }
    }

    /// <summary>
    /// Checks the DeadZone of the object Axis
    /// </summary>
    /// <param name="deadZone"> The deadZone passed in</param>
    private Vector2 CheckDeadZoneAxis(Vector2 axisValues, Vector2 deadZone)
    {
        // The deadzone is (0.2f, 0.1f)
        if (axisValues.x > -deadZone.x && axisValues.x < deadZone.x)
        {
            axisValues.x = 0f;                              
        }
        if (axisValues.y > -deadZone.y && axisValues.y < deadZone.y)
        {            
            axisValues.y = 0f;            
        }
        //Debug.Log("DeadZone is: " + deadZone.ToString());
        //Debug.Log("axisValues are: " + axisValues.ToString());
        return axisValues;
    }

    /// <summary>
    /// Private function to rotate the object, depending on the options passed in
    /// </summary>
    /// <param name="rotationToApply"> The eulerAngles rotation to apply</param>
    /// <param name="rotationOptions"> The options to apply to the rotation</param>
    private void RotateObject(Vector3 rotationToApply, RotationOptionsEnum rotationOptions)
    {
        // We switch throught the option chosen and apply the rotation according to it
        switch (rotationOptions)
        {
            case RotationOptionsEnum.All:
                m_ObjectManager.ObjectTransform.eulerAngles = rotationToApply;
                break;
            case RotationOptionsEnum.xy:
                m_ObjectManager.ObjectTransform.eulerAngles = new Vector3(rotationToApply.x, rotationToApply.y, m_ObjectManager.ObjectTransform.eulerAngles.z);
                break;
            case RotationOptionsEnum.x:
                m_ObjectManager.ObjectTransform.eulerAngles = new Vector3(rotationToApply.x, m_ObjectManager.ObjectTransform.eulerAngles.y, m_ObjectManager.ObjectTransform.eulerAngles.z);
                break;
            case RotationOptionsEnum.y:
                m_ObjectManager.ObjectTransform.eulerAngles = new Vector3(m_ObjectManager.ObjectTransform.eulerAngles.x, rotationToApply.y, m_ObjectManager.ObjectTransform.eulerAngles.z);
                break;
            case RotationOptionsEnum.z:
                m_ObjectManager.ObjectTransform.eulerAngles = new Vector3(m_ObjectManager.ObjectTransform.eulerAngles.x, m_ObjectManager.ObjectTransform.eulerAngles.y, rotationToApply.z);
                break;
            default:
                break;
        }

    }

    /// <summary>
    /// Makes the current object face the objective
    /// </summary>
    /// <param name="objective"> The point to look at in world coordinates</param>
    public void LookAt(Vector3 objective)
    {
        // If the current object is moving with a navMeshAgent...
        if (m_ObjectManager.MovementController.TypeOfMovement == MovementController.TypeOfMovementEnum.NavMeshAgent)
        {
            // ... We deactivate the automatic rotation update from the navMeshAgent
            m_ObjectManager.MovementController.NavMAgent.updateRotation = false;            
        }
        m_ObjectManager.ObjectTransform.LookAt(objective);
    }

    /// <summary>
    /// [NOT IMPLEMENTED] Makes the transform origin to face the provided direction 
    /// </summary>
    /// <param name="origin"> The Transform to rotate </param>
    /// <param name="dir"> The direction to look at</param>
    public void LookAt(Transform origin, Vector3 dir)
    {
        throw new UnityException("LookAt(dir) not implemented!");
    }

    /// <summary>
    /// [NOT IMPLEMENTED] Makes origin face the objective
    /// </summary>
    /// <param name="origin"> The transform to rotate</param>
    /// <param name="objective"> The transform objective to face</param>
    public void LookAt(Transform origin, Transform objective)
    {
        throw new UnityException("LookAt(transform, transform) not implemented!");
    }

    /// <summary>
    /// Stops controlling the rotation of the current object, giving back control to other sources (navmeshAgent, etc)
    /// </summary>
    public void StopControlRotation()
    {
        // If the controller is actually controlling the current object
        if (m_ControlRotation)
        {
            // If the navMeshAgent is not controlling the rotation...
            if (m_ObjectManager.MovementController.NavMAgent.updateRotation == false)
            {
                // ... we give it back the rotation
                m_ObjectManager.MovementController.NavMAgent.updateRotation = true;
            }

            m_ControlRotation = false;
        }
    }

    /// <summary>
    /// Public function to reset the rotation in the controller
    /// </summary>
    public void ResetRotation ()
    {
        // We set the internal rotation to the intial one
        m_RotationObject = m_InitialRotation;
        // We update the rotation based on the reseted internal rotation
        RotateObject(m_RotationObject, RotationOptionsEnum.All);

    }

}
