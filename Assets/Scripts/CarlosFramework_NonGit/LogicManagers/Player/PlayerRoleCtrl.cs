﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Handles the logic of the role of the players
/// </summary>
public class PlayerRoleCtrl : MonoBehaviour {

    /// <summary>
    /// (Field) Identifies the player (player1, player2, etc) at every moment
    /// </summary>
    [SerializeField]
    private int m_PlayerID;
	/// <summary>
	/// (Property) Identifies the player (player1, player2, etc) at every moment
	/// </summary>
	/// <value>The player ID.</value>
	public int PlayerID { get { return m_PlayerID; } set { this.m_PlayerID = value; } }

    /// <summary>
    /// (Field) The material of the player
    /// </summary>
    [SerializeField]
    private Material m_PlayerMat;

    /// <summary>
    /// (Field) The color assigned to this player
    /// </summary>
	//[SerializeField, Tooltip ("DO NOT MODIFY! Debug Only")]
    private Color m_PlayerColor;
	/// <summary>
	/// (Property) The color assigned to this player
	/// </summary>
	/// <value>The color of the player.</value>
	public Color PlayerColor { get { return m_PlayerColor; } }

	/// <summary>
	/// Enumeration of all the possible colors of the player
	/// </summary>
	public enum PlayerColorsEnum {
		Red, 
		Green, 
		Blue,
		Cyan,
		Yellow
	}
	/// <summary>
	/// The ID of the color of the player (predefined)
	/// </summary>
	[SerializeField, Tooltip("Set is at the beggining of the game only! DO NOT modify in runtime!")]
	private PlayerColorsEnum m_PlayerColorID;
	/// <summary>
	/// (Property) The ID of the color of the player (predefined)
	/// </summary>
	/// <value>The player color ID.</value>
	public PlayerColorsEnum PlayerColorID {get { return m_PlayerColorID; } }

	/// <summary>
	/// The ID that is currently controlling this player
	/// </summary>
	[SerializeField, Tooltip("DO NOT MODIFY! Shows who is controlling this player now")]
	private int m_CurrentControlID;
	/// <summary>
	/// The ID that is currently controlling this player (if it matches to m_PlayerID, is itself. If not, is another one)
	/// </summary>
	public int CurrentControlID { get { return m_CurrentControlID; } set { this.m_CurrentControlID = value; } }

    /// <summary>
    /// Enumeration of roles
    /// </summary>
    public enum PlayerRolesEnum
    {
        StickFigure,
        Ball
    }
    /// <summary>
    /// The actual player role. This will indicate that Player with ID n will have that role at this moment. 
    /// </summary>
	[SerializeField]
	private PlayerRolesEnum m_PlayerCurrentRole;
	/// <summary>
	/// The actual player role. This will indicate that Player with ID n will have that role at this moment.
	/// </summary>
	public PlayerRolesEnum PlayerCurrentRole { get { return m_PlayerCurrentRole; } }


	/// <summary>
	/// Sets the color of the player based on the ID passed in
	/// </summary>
	/// <param name="colorID">Color I.</param>
	private void SetPlayerColor(PlayerColorsEnum colorID) {
		// We switch the id passed in and assign the color depending on it
		switch (colorID) {
		case PlayerColorsEnum.Blue:
			m_PlayerColor = Color.blue;
			break;
		case PlayerColorsEnum.Cyan:
			m_PlayerColor = Color.cyan;
			break;
		case PlayerColorsEnum.Green:
			m_PlayerColor = Color.green;
			break;
		case PlayerColorsEnum.Red:
			m_PlayerColor = Color.red;
			break;
		case PlayerColorsEnum.Yellow:
			m_PlayerColor = Color.yellow;
			break;
		default:
			break;
		}

		// We assign the color to the material
		m_PlayerMat.color = m_PlayerColor;
	}

	/// <summary>
	/// Restarts the color of the player to the original one.
	/// </summary>
	public void RestartPlayerColor () {
		// Restart visual color
		SetPlayerColor (m_PlayerColorID);

		// Restart current control ID (the player control itself)
		m_CurrentControlID = m_PlayerID;

		// Restart current role (it should math the playerId at the beginning)
		SetPlayerRole ((PlayerRolesEnum) PlayerID);
	}

	/// <summary>
	/// Applies the player color ID in the component
	/// </summary>
	public void ApplyPlayerColorID() {
		// We call SetPlayercolor with the internal current specifyied color
		SetPlayerColor(m_PlayerColorID);
	}

    /// <summary>
    /// Sets the role of the player, based on the value passed in
    /// </summary>
    /// <param name="role">The role to set the player </param>
    public void SetPlayerRole (PlayerRolesEnum role)
    {
        // Sets the role passed in
        m_PlayerCurrentRole = role;

        // Access the specified avatar [(int)role] and change its color to our current player color
        // [NEEDS REFACTOR. IT IS NOT GENERIC!!!]
        int index = (int)role;
        (Toolbox.Instance.GameManager.GameLogicController as SticksNRoses_GameLogic).Materials[index].color = m_PlayerColor;
		// We also change its current Control color ID to ours
		// [DIRTY CODE. IT SHOULD BE DONE IN A MORE GENERIC WAY, AND CONTROLLED FROM A METHOD INSIDE PlayerRoleCtrl]
		//(Toolbox.Instance.GameManager.GameLogicController as SticksNRoses_GameLogic).Players[index].GetComponent<PlayerManager>().PlayerRoleCtrl.m_CurrentControlID = m_PlayerID;
		Toolbox.Instance.GameManager.Players[index].PlayerRoleCtrl.m_CurrentControlID = m_PlayerID;

    }

    /// <summary>
    /// Switches to the other binary role
    /// </summary>
    public void SwitchPlayerRole ()
    {
        // If it is currently stickFigure, switches to ball. And viceversa
        switch (m_PlayerCurrentRole)
        {
            case PlayerRolesEnum.StickFigure:
                SetPlayerRole(PlayerRolesEnum.Ball);
                break;
            case PlayerRolesEnum.Ball:
                SetPlayerRole(PlayerRolesEnum.StickFigure);
                break;
            default:
                break;
        }
    }
	
}
