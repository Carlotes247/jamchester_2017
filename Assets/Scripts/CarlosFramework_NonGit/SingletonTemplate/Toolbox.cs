﻿//---------------------------------------------------------------------------
// Carlos Gonzalez Diaz - TFG - Simulador Virtual Carabina M4 - 2016
// Universidad Rey Juan Carlos - ETSII
//---------------------------------------------------------------------------

/*! \file Toolbox.cs
 *  \brief The toolbox is a singleton, but it improves upon the concept.
 * 
 *  The toolbox is a singleton, before anything else. But it improves upon the concept.
 *  Basically this encourages better coding practices, such as reducing coupling and unit testing.
 *  
 *  Implementation extracted from http://wiki.unity3d.com/index.php/Toolbox
 *  
 * The toolbox creates the Game Manager and instantiates it as a child, giving it all the benefits from a Singleton!
 * */

using UnityEngine;


/// <summary>
/// The toolbox is a singleton, but it improves upon the concept. (access the Instance)
/// </summary>
public class Toolbox : Singleton<Toolbox> {
	protected Toolbox () {} // guarantee this will be always a singleton only - can't use the constructor!

    [SerializeField]
    private GameObject gameManagerObject;
    /// <summary>
    /// The gameManager to instantiate [DON'T ACCESS]
    /// </summary>
    public GameObject GameManagerObject { get { return this.gameManagerObject; } set { this.gameManagerObject = value; } }

    [SerializeField]
    private GameManager gameManager;
    /// <summary>
    /// The GameManager of the game. It contains the controllers of the logic of the game
    /// </summary>
    public GameManager GameManager { get { return this.gameManager; } set { this.gameManager = value; } }


    /// <summary>
    /// Definition of the enum of the location of the GameManager
    /// </summary>
    public enum GameManagerLocationEnum
    {
        Prefab,
        Scene
    }
    /// <summary>
    /// (Field) The location of the GameManager
    /// </summary>  
    private GameManagerLocationEnum m_GameManagerLocation;
    /// <summary>
    /// (Property) The location of the GameManager
    /// </summary>
    public GameManagerLocationEnum GameManagerLocation { get { return this.m_GameManagerLocation; } set { this.m_GameManagerLocation = value; } }


    /// The variable that will be the direct child of the toolbox and benefit from all its properties.
    //private GameObject directChild;

    // I think that my global var is for creating whatever object we want with the toolbox
    //public GameObject myGlobalVar;
 
	//public Language language = new Language();
 
	void Awake () {
		// Your initialization code here
        //Debug.Log("Toolbox launched");
        // The toolbox will create the instance of the gameManager! Awesome!
	}

    public void Start()
    {
        // We load the gameManager from the scene or from a prefab depending on the enum
        switch (m_GameManagerLocation)
        {
            case GameManagerLocationEnum.Prefab:
                LoadGameManagerPrefab();
                break;
            case GameManagerLocationEnum.Scene:
                LoadGameManagerScene();
                break;
            default:
                break;
        }

        

    }

    /// <summary>
    /// Loads the GameManager from Resources as a prefab
    /// </summary>
    private void LoadGameManagerPrefab ()
    {
        // We ensure that the gameManager is not already instantiated
        if (GameManager == null)
        {
            Debug.Log(this.name + ": Loading GameManager from Resources");
            //GameManagerObject = Instantiate(GameManagerObject) as GameObject;
            GameManagerObject = Instantiate(Resources.Load("GameManager")) as GameObject;
            //gameManager = Instantiate(Resources.Load("GameManager")) as GameObject;
            //Debug.Log("Game Manager instantiated");
            GameManagerObject.transform.parent = this.transform;
            GameManager = GameManagerObject.GetComponent<GameManager>();
            // We make sure that the GameManagerObject is always active when the game starts
            GameManagerObject.SetActive(true);
            //Debug.Log("Game Manager is now a child of Toolbox");
            //// We create the directChild in the scene
            //directChild = new GameObject("DirectChild");
            //// We set the directChild as a child in the inspector, once the toolbox has been loaded
            //directChild.transform.SetParent(this.transform);
            //// We then instantiate the gameManager as the child of the Toolbox
            //directChild = Instantiate(gameManager);
        }
    }

    /// <summary>
    /// Loads the GameManager from the Scene, searching for it first
    /// </summary>
    private void LoadGameManagerScene ()
    {
        Debug.Log(this.name + ": Loading GameManager from the Scene");

        //// We find the GameObject with Tag GameManager in the scene, and we load it
        //GameManagerObject = GameObject.FindWithTag("GameManager");
        
        //// We deactivate the GameManager in the scene, leaving only one active (the on in the toolbox)
        //GameObject.FindWithTag("GameManager").SetActive(false);

        // We already have the GameManager object in the scene from the GameLauncher

        // We set it as the child of the toolbox
        GameManagerObject.transform.parent = this.transform;
        // We make sure that the GameManagerObject is always active when the game starts
        GameManagerObject.SetActive(true);
        // We get the GameManager component from the object
        GameManager = GameManagerObject.GetComponent<GameManager>();


    }

    // (optional) allow runtime registration of global objects
    /*static public T RegisterComponent<T> () {
        //return Instance.GetComponent<T>();
		return Instance.GetOrAddComponent<T>();
	}*/
}
 
/*[System.Serializable]
public class Language {
	public string current;
	public string lastLang;
}*/