﻿#if !UNITY_WEBGL
#define NON_WEBGL_BUILD
#endif


using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LD_LogicCtrl : GameLogicController {

    #region Attributes&Properties

    /// <summary>
	/// The index of the level to load first
	/// </summary>
	[SerializeField, Tooltip("Check Build Settings for level 1")]
    private int m_LevelToLoadFirst;

    /// <summary>
    /// (Field) True if the game is lost
    /// </summary>
    private bool m_LooseFlag;
    /// <summary>
    /// (Property) True if the game is lost
    /// </summary>
    public override bool LooseFlag { get { return this.m_LooseFlag; } }

    /// <summary>
    /// (Field) True if the game is won
    /// </summary>
    private bool m_WinFlag;
    /// <summary>
    /// (Property) True if the game is won
    /// </summary>
    public override bool WinFlag { get { return this.m_WinFlag; } }

    /// <summary>
    /// The array of moves available
    /// </summary>
    [SerializeField]
    private PSMoveController[] m_Moves;
    public PSMoveController[] Moves { get { return m_Moves; } }


#endregion

    #region UnityFunctions
    // Use this for initialization
    void Start()
    {
        // We initalize the game
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {

    }
    #endregion

    #region GameLogicFunctions

    /// <summary>
    /// Initializes the component to the default values
    /// </summary>
    public void Initialize()
    {
        // We set all the flags to false
        m_LooseFlag = false;
        m_WinFlag = false;

        // Hide the EndGame Text
        Toolbox.Instance.GameManager.HudController.EndLevelUI.HideUI();

        // We open the mainMenu
        Toolbox.Instance.GameManager.MenuController.OpenMainMenu(true);

    }

    public override void Loose()
    {
        throw new NotImplementedException();
    }

    public override void SetPlayerAtInitialPosition(Vector3 pos)
    {
        //Debug.Log("SetPlayersAtInitialPosition called, but we do nothing for this game!");
        //Toolbox.Instance.GameManager.Player.MovementController.Teleport(pos);

    }

    public override void SetPlayersAtInitialPosition(Vector3[] pos)
    {
        Debug.Log("SetPlayersAtInitialPosition called, but we do nothing for this game!");
    }

    /// <summary>
    /// Starts the game
    /// </summary>
    public override void StartGame()
    {
        // We don't execute if the GameManager is not instantiated
        if (Toolbox.Instance.GameManager == null)
        {
            return;
        }

        if (Toolbox.Instance.GameManager.MenuController != null)
        {
            // We close the Main Menu
            Toolbox.Instance.GameManager.MenuController.OpenMainMenu(false);
        }

        if (Toolbox.Instance.GameManager.GameLevelController != null)
        {
            // We load the first level
            Toolbox.Instance.GameManager.GameLevelController.LoadGameLevel(m_LevelToLoadFirst);
        }
    }

    public override void Win()
    {
        if (!m_WinFlag)
        {
            // If all the levels have been completed...
            if (Array.TrueForAll(Toolbox.Instance.GameManager.GameLevelController.GameLevels, GameLevelLogic.CheckLevelCompleted))
            {
                // We set the win flag to true
                m_WinFlag = true;
                Debug.Log("Game Won");
                Toolbox.Instance.GameManager.HudController.UpdateStoryText("Game Won");

                // We perform the end Game logic
                WinLogic();

            }
            // If there are levels left to complete...
            else
            {
                Debug.Log("There are still levels left to complete!");
                Toolbox.Instance.GameManager.HudController.UpdateStoryText("There are still levels left to complete!");
            }

            // end/win the level
            int currentLevel = Toolbox.Instance.GameManager.GameLevelController.IndexCurrentLevel;
            Debug.Log("win GAME called, curentlevelindex is: " + currentLevel);
            //Toolbox.Instance.GameManager.GameLevelController.GameLevels[currentLevel].GetComponent<GameLevelLogic>().WinLevel();
        }
    }

    /// <summary>
    /// The logic to do when the game is won
    /// </summary>
    private void WinLogic()
    {
        // TO DO
        // Show win screen
        Toolbox.Instance.GameManager.HudController.EndGameUI.ShowUI("");
    }

    public override void Win(int playerID)
    {
        Win();
    }

    /// <summary>
    /// Saves the state of the original game.
    /// </summary>
    public void RestartGame()
    {
        // We set all the flags to false
        m_LooseFlag = false;
        m_WinFlag = false;

        // Hide the EndLevel Text
        Toolbox.Instance.GameManager.HudController.EndLevelUI.HideUI();
        // Hide the EndGame Text
        Toolbox.Instance.GameManager.HudController.EndGameUI.HideUI();

        // We restart the levels
        RestartLevels();

    }



    /// <summary>
    /// Restarts the levels.
    /// </summary>
    private void RestartLevels()
    {
        // We go through the levels. We start in 1 because 0 is the main menu
        for (int i = 1; i < Toolbox.Instance.GameManager.GameLevelController.GameLevels.Length; i++)
        {
            // We restart our levels
            Toolbox.Instance.GameManager.GameLevelController.GameLevels[i].GetComponent<GameLevelLogic>().RestartLevelState();
        }
    }

    #endregion

}
